package Game;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Core.ScriptLoader;
import GUI.GUILoader;
import GUI.GUITitle;
import GUI.GuiParameterSet;
import Xeon.Xeon;

public class WaveManager {

	private Game game;
	private Xeon xeon;
	
	private ArrayList<Wave> waveList = new ArrayList<Wave>();
	private int currentWave = 0;
	
	//Used to diplay messages
	private int delay = 0;
	private int messageDelay = 0;
	private boolean hasToRemoveMessage = false;
	
	private List<String> cachedScript = new ArrayList<String>();
	
	public WaveManager(Game game,Xeon xeon){
		
		this.game=game;
		this.xeon=xeon;
		
	}
	
	public void resetGame(){
		for(int i = 0; i < waveList.size(); i++){
			waveList.get(i).setCurrentPush(0);
			waveList.get(i).setPreConsumed(false);
			waveList.get(i).setPostConsumed(false);
		}
		currentWave = 0;
	}
	
	public void loadWaves(File f){
		
		ScriptLoader sl = new ScriptLoader();
		
		cachedScript = sl.loadScript(f);
		
		Wave currentWave = null;
		Push currentPush = null;
		
		for(int i = 0; i < cachedScript.size(); i++){
			
			if(cachedScript.get(i).startsWith("[Wave]")){
				currentWave = new Wave(game, xeon,this);
				waveList.add(currentWave);
			}
			if(cachedScript.get(i).startsWith("[Push]")){
				currentPush = new Push(game);
				if(currentWave != null){
					currentWave.getPushList().add(currentPush);
				}
			}
			if(cachedScript.get(i).startsWith("/")){
				if(currentWave != null && currentPush != null){
					currentPush.getShipList().add(Integer.parseInt(cachedScript.get(i).replace("/", "")));
				}
			}
			if(cachedScript.get(i).startsWith("rad:")){
				if(currentWave != null && currentPush != null){
					currentPush.setRadius(Integer.parseInt(cachedScript.get(i).replace("rad:", "")));
				}
			}
			if(cachedScript.get(i).startsWith("<PRE>")){
				if(currentWave != null){
					currentWave.setPreTitle(cachedScript.get(i).replace("<PRE>", ""));
				}
			}
			if(cachedScript.get(i).startsWith("<PRE,UNDER>")){
				if(currentWave != null){
					currentWave.setPreUnderTitle(cachedScript.get(i).replace("<PRE,UNDER>", ""));
				}
			}
			if(cachedScript.get(i).startsWith("<POS>")){
				if(currentWave != null){
					currentWave.setPostTitle(cachedScript.get(i).replace("<POS>", ""));
				}
			}
			if(cachedScript.get(i).startsWith("<POS,UNDER>")){
				if(currentWave != null){
					currentWave.setPostUnderTitle(cachedScript.get(i).replace("<POS,UNDER>", ""));
				}
			}
			if(cachedScript.get(i).startsWith("<DEL>")){
				if(currentWave != null){
					currentWave.setPostDelay(Integer.parseInt(cachedScript.get(i).replace("<DEL>", "")));
				}
			}
			
		}
		
	}
	
	public void next(){
		
		if(hasToRemoveMessage && messageDelay == 0){
			hasToRemoveMessage = false;
			removeMesage();
		}else if(messageDelay > 0){
			messageDelay--;
			return;
		}
		
		if(delay > 0){
			delay--;
			return;
		}
		
		if(currentWave>waveList.size()-1){
			return;
		}
		
		while(!waveList.get(currentWave).next()){
			currentWave ++;
			if(currentWave>waveList.size()-1){
				//Fin du jeu
				break;
			}
		}
		
	}
	
	public void message(String title,String underTitle){
		
		System.out.println("MESSAGE : " + title + " " + underTitle);
		
		if(!title.equals("")){
			GuiParameterSet gps = new GuiParameterSet("");
			gps.setTextColor(Color.white);
			gps.setW(100);
			gps.setH(100);
			gps.setX(0);
			gps.setY(0);
			gps.setText(title);
			gps.setFont("Venus");
			gps.setId("HUD_MESSAGE_TITLE");
			gps.setTextSize(70F);
			GUITitle titleGUI = new GUITitle(xeon, gps);
			
			xeon.getWorldManager().getActiveWorld().getGui().getGuiLoader().getGuiElementList().add(titleGUI);
		
			hasToRemoveMessage = true;
			messageDelay = 5;
		}
		
		if(!underTitle.equals("")){
			GuiParameterSet gps = new GuiParameterSet("");
			gps.setTextColor(Color.white);
			gps.setW(100);
			gps.setH(50);
			gps.setX(0);
			gps.setY(50);
			gps.setText(underTitle);
			gps.setFont("Venus");
			gps.setId("HUD_MESSAGE_UNDERTITLE");
			gps.setTextSize(40F);
			GUITitle titleGUI = new GUITitle(xeon, gps);
			
			xeon.getWorldManager().getActiveWorld().getGui().getGuiLoader().getGuiElementList().add(titleGUI);
		
			hasToRemoveMessage = true;
			messageDelay = 5;
		}
		
	}
	
	public void removeMesage(){
		
		GUILoader gl = xeon.getWorldManager().getActiveWorld().getGui().getGuiLoader();
		
		for(int i = 0; i < gl.getGuiElementList().size(); i++){
			
			if(gl.getGuiElementList().get(i).getElementName().equals("HUD_MESSAGE_TITLE") || gl.getGuiElementList().get(i).getElementName().equals("HUD_MESSAGE_UNDERTITLE")){
				gl.getGuiElementList().remove(i);
				if(i>0){
					i--;
				}
			}
			
		}
		
	}

	public ArrayList<Wave> getWaveList() {
		return waveList;
	}

	public void setWaveList(ArrayList<Wave> waveList) {
		this.waveList = waveList;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}
	
	
	
}
