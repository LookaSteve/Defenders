package Game;

import java.awt.Color;
import java.util.Random;

import Entities.Ship;
import Entity.Entity;
import Event.CollisionEvent;
import GUI.GUITick;
import GUI.GUITitle;
import GUI.GuiParameterSet;
import Xeon.Xeon;

public class Collision {
	
	private Game game;
	private Xeon xeon;
	
	public Collision(Game game,Xeon xeon){
		
		this.game = game;
		this.xeon = xeon;
		
	}

	public void collide(CollisionEvent event){
		
		if(event.getEntityA().getEntityType().equals("Player_Laser") && event.getEntityB().getEntityType().equals("Enemy_Ship")){
			
			event.getEntityA().kill();

			damageShip(event.getEntityB(),30,true);
			
			game.setScore(game.getScore() + 1);
			
			game.addSpecial(1);
			
			return;
			
		}
		
		if(event.getEntityA().getEntityName().equals("Ion_Blast") && event.getEntityB().getEntityType().equals("Enemy_Ship")){
			
			damageShip(event.getEntityB(),2,false);
			
			return;
			
		}
		
		if(event.getEntityA().getEntityType().equals("Player_Misile") && event.getEntityB().getEntityType().equals("Enemy_Ship")){
			
			if(!event.getEntityA().isTargetForDeletion()){
				game.playSound("Ion");
				game.getFx().ionBlast(event.getEntityA());
			}
			
			event.getEntityA().kill();
			
			return;
			
		}
		
		if(event.getEntityA().getEntityType().equals("Enemy_Laser") && event.getEntityB().getEntityName().equals("XEON_HITBOX_CAMERA")){
			
			event.getEntityA().kill();
			if(event.getEntityA().getEntityName().equals("Power_Laser")){
				game.setLife(game.getLife()-60);
			}
			if(event.getEntityA().getEntityName().equals("Laser")){
				game.setLife(game.getLife()-30);
			}
			
			if(game.getLife() <= 0){
				event.getEntityB().kill();
				game.getFx().explode(event.getEntityB(),true);
				createGameOver();
			}else{
				game.getFx().bulletHit(event.getEntityA(),true);
			}
			
			return;
			
		}
		
		if(event.getEntityA().getEntityType().equals("Bonus") && event.getEntityB().getEntityName().equals("XEON_HITBOX_CAMERA")){
			
			event.getEntityA().kill();
			if(event.getEntityA().getEntityName().equals("Health_Bonus")){
				game.setLife(game.getLife()+60);
				if(game.getLife()>game.getDefaultLife()){
					game.setLife(game.getDefaultLife());
				}
				return;
			}
			if(event.getEntityA().getEntityName().equals("Missile_Bonus")){
				
			}
			
		}
		
		
	}
	
	public void damageShip(Entity e,int damage,boolean isDamageVisible){
		
		Ship s = (Ship) e;
		if(damage>=s.getLife()){
			if(e.getEntityType().equals("Enemy_Ship") && !e.isTargetForDeletion()){
				game.setScore(game.getScore() + 100);
				game.playSound("Boom");
				game.addSpecial(10);
				Random rand = new Random();
				if(rand.nextInt(2)==1){
					game.getFx().createHealthBonus(e);
				}
			}
			e.kill();
			game.getFx().explode(e,true);
		}else{
			s.setLife(s.getLife()-damage);
			if(isDamageVisible){
				game.getFx().bulletHit(e,false);
			}
		}
		
	}
	
	public void createGameOver(){
		
		GuiParameterSet gps = new GuiParameterSet("");
		gps.setTextColor(Color.red);
		gps.setW(100);
		gps.setH(100);
		gps.setX(0);
		gps.setY(0);
		gps.setText("Game_Over");
		gps.setFont("Venus");
		gps.setTextSize(70F);
		GUITitle title = new GUITitle(xeon, gps);
		
		xeon.getWorldManager().getActiveWorld().getGui().getGuiLoader().getGuiElementList().add(title);
		xeon.getAssetManager().stopAllMusic();
		game.playMusicLoop("Game_Over",false);
		
	}
	
}
