package Game;

import java.util.Random;

import Entity.Entity;
import Entity.EntityParameterSet;
import Xeon.Xeon;

public class Fx {

	private Xeon xeon;
	
	public Fx(Xeon xeon) {
		
		this.xeon = xeon;
		
	}
	
	public void bulletHit(Entity e,boolean withDebris){
		
		EntityParameterSet eps = new EntityParameterSet();
		eps.setX(e.getX() - e.getW()/2);
		eps.setY(e.getY() - e.getH()/2);
		eps.setW(50);
		eps.setH(50);
		eps.setDoclip(false);
		eps.setEntityName("Small_Explosion");
		eps.setEntityType("Effect");
		eps.setAnimation("Small_Explosion");
		eps.setRotation(e.getRotation());
		eps.setEntityPhysic(Entity.PHYSIC_NOCLIP);
		eps.setTimeToLive(50);
		Entity explosion = new Entity(xeon,eps);
		xeon.getEntityManager().addEntity(explosion);
		
		if(withDebris){
			generateRandomDebris(e,1);
		}
		
	}
	
	public void ionBlast(Entity e){
		
		EntityParameterSet eps = new EntityParameterSet();
		eps.setX(e.getX() - e.getW()/2);
		eps.setY(e.getY() - e.getH()/2);
		eps.setW(100);
		eps.setH(100);
		eps.setDoclip(false);
		eps.setEntityName("Ion_Blast");
		eps.setEntityType("Effect");
		eps.setAnimation("Ion_Blast");
		eps.setRotation(e.getRotation());
		eps.setEntityPhysic(Entity.PHYSIC_NOCLIP);
		eps.setTimeToLive(70);
		Entity explosion = new Entity(xeon,eps);
		xeon.getEntityManager().addEntity(explosion);
		
	}
	
	public void explode(Entity e,boolean withDebris){
		
		EntityParameterSet eps = new EntityParameterSet();
		eps.setX(e.getX() - e.getW()/2);
		eps.setY(e.getY() - e.getH()/2);
		eps.setW(100);
		eps.setH(100);
		eps.setDoclip(false);
		eps.setEntityName("Explosion");
		eps.setEntityType("Effect");
		eps.setAnimation("Explosion");
		eps.setRotation(e.getRotation());
		eps.setEntityPhysic(Entity.PHYSIC_NOCLIP);
		eps.setTimeToLive(100);
		Entity explosion = new Entity(xeon,eps);
		xeon.getEntityManager().addEntity(explosion);
		
		if(withDebris){
			Random rand = new Random();
			generateRandomDebris(e,rand.nextInt(3));
		}
		
	}
	
	public void createHealthBonus(Entity e){
		EntityParameterSet eps = new EntityParameterSet();
		eps.setX(e.getX() - e.getW()/2);
		eps.setY(e.getY() - e.getH()/2);
		eps.setW(100);
		eps.setH(100);
		eps.setDoclip(false);
		eps.setEntityName("Health_Bonus");
		eps.setEntityType("Bonus");
		eps.setAnimation("Health_Bonus");
		eps.setRotation(e.getRotation());
		eps.setEntityPhysic(Entity.PHYSIC_NOCLIP);
		eps.setTimeToLive(1000);
		Entity healthBonus = new Entity(xeon,eps);
		healthBonus.getAnimation().setDoRepeat(true);
		xeon.getEntityManager().addEntity(healthBonus);
	}
	
	public void generateRandomDebris(Entity e,int amount){
		
		int[] w = {10,22,28,14,8,8}; 
		int[] h = {10,14,20,14,8,6}; 
		String name = "Debris_";
		
		for(int i = 0; i < amount; i++){
			
			Random rand = new Random();
			int id = rand.nextInt(6);
			int ttl = rand.nextInt(400);
			float speed = (float) (rand.nextFloat()*10); 
			float angle = (float) (rand.nextFloat()*2*Math.PI); 
			
			EntityParameterSet eps = new EntityParameterSet();
			eps.setX(e.getX());
			eps.setY(e.getY());
			eps.setW(w[id]);
			eps.setH(h[id]);
			eps.setDoclip(false);
			eps.setEntityName("Debris");
			eps.setEntityType("Effect");
			eps.setTexture(name + (id+1));
			eps.setRotation(angle);
			eps.setEntityPhysic(Entity.PHYSIC_NOCLIP);
			eps.setTimeToLive(ttl);
			Entity entity = new Entity(xeon, eps);
			xeon.getProjectionAssistant().projectAtRandomAngle(entity, speed, 0);
			xeon.getEntityManager().addEntity(entity);
		}
		
		
		
	}
	
}
