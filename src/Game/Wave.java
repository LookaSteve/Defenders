package Game;

import java.util.ArrayList;

import Xeon.Xeon;

public class Wave {

	private Game game;
	private Xeon xeon;
	private WaveManager waveManager;
	
	private ArrayList<Push> pushList = new ArrayList<Push>();
	int currentPush = 0;
	
	private String preTitle = "";
	private String postTitle = "";
	private String preUnderTitle = "";
	private String postUnderTitle = "";
	
	private boolean isPreConsumed = false;
	private boolean isPostConsumed = false;
	
	private int postDelay = 0;
	
	public Wave(Game game, Xeon xeon, WaveManager waveManager){
		
		this.game = game;
		this.xeon = xeon;
		this.waveManager = waveManager;
		
	}
	
	public boolean next(){
		
		if(!isPreConsumed){
			isPreConsumed = true;
			waveManager.message(preTitle, preUnderTitle);
			return true;
		}
		
		if(currentPush>pushList.size()-1){
			if(!isPostConsumed){
				isPostConsumed = true;
				waveManager.message(postTitle, postUnderTitle);
				waveManager.setDelay(postDelay);
				return true;
			}
			return false;
		}else{
			pushList.get(currentPush).push();
			currentPush++;
		}
		return true;
		
	}

	public ArrayList<Push> getPushList() {
		return pushList;
	}

	public void setPushList(ArrayList<Push> pushList) {
		this.pushList = pushList;
	}

	public int getCurrentPush() {
		return currentPush;
	}

	public void setCurrentPush(int currentPush) {
		this.currentPush = currentPush;
	}

	public String getPreTitle() {
		return preTitle;
	}

	public void setPreTitle(String preTitle) {
		this.preTitle = preTitle;
	}

	public String getPostTitle() {
		return postTitle;
	}

	public void setPostTitle(String postTitle) {
		this.postTitle = postTitle;
	}

	public String getPreUnderTitle() {
		return preUnderTitle;
	}

	public void setPreUnderTitle(String preUnderTitle) {
		this.preUnderTitle = preUnderTitle;
	}

	public String getPostUnderTitle() {
		return postUnderTitle;
	}

	public void setPostUnderTitle(String postUnderTitle) {
		this.postUnderTitle = postUnderTitle;
	}

	public boolean isPreConsumed() {
		return isPreConsumed;
	}

	public void setPreConsumed(boolean isPreConsumed) {
		this.isPreConsumed = isPreConsumed;
	}

	public int getPostDelay() {
		return postDelay;
	}

	public void setPostDelay(int postDelay) {
		this.postDelay = postDelay;
	}

	public boolean isPostConsumed() {
		return isPostConsumed;
	}

	public void setPostConsumed(boolean isPostConsumed) {
		this.isPostConsumed = isPostConsumed;
	}
	
	
	
}
