package Game;

import java.awt.Color;
import java.util.Random;

import Assets.AnimationInstance;
import Assets.Light;
import Assets.SoundEffect;
import Entity.Entity;
import World.World;
import Xeon.Xeon;

public class Tick {
	
	private Game game;
	private Xeon xeon;
	
	private int cooldown = 0;
	
	private boolean rotatePositive = true;
	
	public Tick(Game game,Xeon xeon){
		
		this.game = game;
		this.xeon=xeon;
		
	}

	public void tick(){
		
		if(cooldown > 0){
			cooldown--;
		}
		
		if(xeon.getControl().getControlState("XEON_CTRL_ACTION") || xeon.getControl().isLeftClick()){
			
			if(cooldown == 0 && !xeon.getWorldManager().getActiveWorld().getCamera().getFocusedEntity().isTargetForDeletion()){
				
				Entity playerShip = xeon.getWorldManager().getActiveWorld().getCamera().getFocusedEntity();
				if(playerShip != null){
					
					Entity e = xeon.getEntityManager().createEntity(xeon, "Laser","Player_Laser", playerShip.getX(),playerShip.getY(), 19, 7,false, 0, 1, 1, 0.001, "Shot",100,Entity.PHYSIC_NOCLIP);
					playerShip.project(e,0,0, -playerShip.getRotation() + Math.PI/2, 7, 0);
					xeon.getEntityManager().addEntity(e);
					Entity e1 = xeon.getEntityManager().createEntity(xeon, "Laser","Player_Laser", playerShip.getX(),playerShip.getY(), 19, 7,false, 0, 1, 1, 0.001, "Shot",100,Entity.PHYSIC_NOCLIP);
					playerShip.project(e1,(int)playerShip.getW(),0, -playerShip.getRotation() + Math.PI/2, 7, 0);
					xeon.getEntityManager().addEntity(e1);
					cooldown = 25;
					game.playSound("Laser");
					
				}	
				
			}
			
		}
		
		if(xeon.getControl().getControlState("XEON_CTRL_SECOND_ACTION") || xeon.getControl().isRightClick()){
			
			if(game.getSpecial()>=100 && !xeon.getWorldManager().getActiveWorld().getCamera().getFocusedEntity().isTargetForDeletion()){
				
				Entity playerShip = xeon.getWorldManager().getActiveWorld().getCamera().getFocusedEntity();
				if(playerShip != null){
					
					Entity e = xeon.getEntityManager().createEntity(xeon, "Misile","Player_Misile", playerShip.getX(),playerShip.getY(), 7, 50,false, 0, 1, 1, 0.001, "Shot",100,Entity.PHYSIC_NOCLIP);
					e.setAnimation(new AnimationInstance(xeon.getAssetManager().getAnimation("Misile"), true));
					e.setTexture(null);
					playerShip.project(e,(int) (playerShip.getW()/2),0, -playerShip.getRotation() + Math.PI/2, 4, 0);
					e.setRotation(playerShip.getRotation());
					xeon.getEntityManager().addEntity(e);
					game.setSpecial(0);
					game.playSound("Missile");
				}	
				
			}
			
		}
		
		game.getIa().IARun();
		
		//Camera slight rotation
		
		World w = xeon.getWorldManager().getActiveWorld();
		
		if(rotatePositive){
			w.getCamera().setRotation(w.getCamera().getRotation()+0.0005);
		}else{
			w.getCamera().setRotation(w.getCamera().getRotation()-0.0005);
		}
		if(game.getLife()> 0){
			if(w.getCamera().getRotation() > 0.1 || w.getCamera().getRotation() < -0.1){
				if(rotatePositive){
					rotatePositive = false;
				}else{
					rotatePositive = true;
				}
			}
		}		
		
	}
	
	public void spawnSwarm(int number,int radius,int type){
		
		Random rand = new Random();
		Entity player = xeon.getWorldManager().getActiveWorld().getCamera().getFocusedEntity();
		
		for(int i = 0; i < number; i ++){
		
			int x = radius - rand.nextInt(radius*2);
			int y = radius - rand.nextInt(radius*2);
			
			if(type == 0){
				xeon.getEntityManager().addEntity(game.getEntityFactory().createSwift((int)(x + player.getX()),(int)(y + player.getY())));
			}
			if(type == 1){
				xeon.getEntityManager().addEntity(game.getEntityFactory().createRaptor((int)(x + player.getX()),(int)(y + player.getY())));
			}
			
			
			
		}
		
	}
	
}
