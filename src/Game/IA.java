package Game;

import java.util.Random;

import Entity.Entity;
import Xeon.Xeon;

public class IA {

	private Game game;
	private Xeon xeon;
	
	public IA(Game game,Xeon xeon){
		
		this.game = game;
		this.xeon = xeon;
		
	}
	
	public void IARun(){
		
		Entity player = xeon.getWorldManager().getActiveWorld().getCamera().getFocusedEntity();
		
		for(int i = 0; i < xeon.getEntityManager().getEntityList().size(); i++){
			
			Entity e = null;
			
			try{
				
				e = xeon.getEntityManager().getEntityList().get(i);
			
				if(e != null){
					
					if(e.getEntityType().equals("Enemy_Ship")){
						
						tickIA(e,player);
						
					}
					
				}
			
			}catch(Exception ex){
				
			}
			
		}
		
	}
	
	public void tickIA(Entity e,Entity player){
		
		if(player == null){
			
			Random rand = new Random();
			int r = rand.nextInt(40);
			
			if(r == 3){
			
				float randA = (float) (rand.nextFloat()*2*Math.PI); 
				xeon.getProjectionAssistant().project(e, randA + Math.PI/2, 4, 0.03);
				
			}
			return;
		}
		
		int distance = (int) Math.sqrt((e.getX()-player.getX())*(e.getX()-player.getX()) + (e.getY()-player.getY())*(e.getY()-player.getY()));
		
		if(player.isTargetForDeletion()){
			
			Random rand = new Random();
			int r = rand.nextInt(40);
			
			if(r == 3){
			
				float randA = (float) (rand.nextFloat()*2*Math.PI); 
				xeon.getProjectionAssistant().project(e, randA + Math.PI/2, 4, 0.03);
				
			}
			
		}else{
			
			double angle = Math.atan2(e.getX() - player.getX(), e.getY() - player.getY());
			
			if(distance > 300){
				
				xeon.getProjectionAssistant().project(e, angle + Math.PI/2, 4, 0.03);
				e.setRotation(-angle);
				
				Random rand = new Random();
				
				int r = rand.nextInt(40);
				
				if(r == 1 && distance < 1000){
					
					attack(e,player);	
					
				}
				
			}else if(distance > 200){
				
				Random rand = new Random();
				
				int r = rand.nextInt(40);
				
				if(r == 3){
				
					float randA = (float) (rand.nextFloat()*2*Math.PI); 
					xeon.getProjectionAssistant().project(e, randA + Math.PI/2, 4, 0.03);
					e.setRotation(-angle);
					
				}
				if(r == 1){
					
					attack(e,player);	
					
				}
				
			}else{
				
				xeon.getProjectionAssistant().project(e, -angle + Math.PI/2, 4, 0.03);
				e.setRotation(-angle);

			}
			
		}
		
		
		
	}
	
	private void attack(Entity e,Entity player){
		if(e.getEntityName().equals("Swift")){
			Entity bullet = xeon.getEntityManager().createEntity(xeon, "Laser","Enemy_Laser", e.getX(),e.getY(), 19, 7,false, 0, 1, 1, 0.001, "Shot",100,Entity.PHYSIC_NOCLIP);
			e.project(bullet,(int) (e.getW()/2),0, -e.getRotation() + Math.PI/2, 7, 0);
			xeon.getEntityManager().addEntity(bullet);
			game.playSound("Laser");
		}
		if(e.getEntityName().equals("Raptor")){
			Entity bullet = xeon.getEntityManager().createEntity(xeon, "Power_Laser","Enemy_Laser", e.getX(),e.getY(), 19, 7,false, 0, 1, 1, 0.001, "Power_Shot",100,Entity.PHYSIC_NOCLIP);
			e.project(bullet,0,0, -e.getRotation() + Math.PI/2, 7, 0);
			xeon.getEntityManager().addEntity(bullet);
			game.playSound("Laser");
			Entity bullet1 = xeon.getEntityManager().createEntity(xeon, "Power_Laser","Enemy_Laser", e.getX(),e.getY(), 19, 7,false, 0, 1, 1, 0.001, "Power_Shot",100,Entity.PHYSIC_NOCLIP);
			e.project(bullet1,(int) e.getW(),0, -e.getRotation() + Math.PI/2, 7, 0);
			xeon.getEntityManager().addEntity(bullet1);
			game.playSound("Laser");
		}
	}
	
}
