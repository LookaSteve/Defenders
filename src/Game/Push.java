package Game;

import java.util.ArrayList;

public class Push {

	private Game game;
	private ArrayList<Integer> shipList = new ArrayList<Integer>();
	private int radius;
	
	public Push(Game game){
		
		this.game = game;
		
	}
	
	public void push(){
		
		for(int i = 0; i < shipList.size(); i++){
			
			game.getTick().spawnSwarm(shipList.get(i), radius,i);
			
		}
		
	}

	public ArrayList<Integer> getShipList() {
		return shipList;
	}

	public void setShipList(ArrayList<Integer> shipList) {
		this.shipList = shipList;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	
	
	
	
}
