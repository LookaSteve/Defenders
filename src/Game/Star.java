package Game;

import java.awt.Color;

public class Star {

	private int x,y,radius;
	private Color color;
	
	public Star(int x,int y,int radius,Color color){
		
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.color = color;
		
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}



	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	
	
}
