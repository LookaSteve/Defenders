package Game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

import Xeon.Xeon;

public class Level {

	private Xeon xeon;
	private Game game;
	
	private int wm,hm;
	
	public Level(Game game,Xeon xeon){
		
		this.xeon = xeon;
		this.game = game;
		
	}
	
	public void drawWorld(Graphics2D g){
		
		if(xeon.isAntiAlliasingActive()){
    		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    	}
		
		int w = xeon.getW()/xeon.getResolutionDiviser();
		int h = xeon.getH()/xeon.getResolutionDiviser();
		if(w != wm || h != hm){
			game.getStarfield().generateField(100);
			wm = w;
			hm = h;
		}
		game.getStarfield().draw(g);
		//Plan des plan�tes
		int px = (int)( -xeon.getWorldManager().getActiveWorld().getCamera().getxPosition()*0.90);
		int py = (int)( -xeon.getWorldManager().getActiveWorld().getCamera().getyPosition()*0.90);
		xeon.getCanvas().renderScaled(g, xeon.getAssetManager().getTexture("Earth"),px+100,py+100, 108, 108, 0.12);
		xeon.getCanvas().renderScaled(g, xeon.getAssetManager().getTexture("Moon"),px-200,py-200, 17, 17, 0.12);
	}
	
	public void drawGUI(Graphics2D g){
		
		if(xeon.isAntiAlliasingActive()){
    		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    	}
		
		int gw = xeon.getW()/xeon.getResolutionDiviser();
		int gh = xeon.getH()/xeon.getResolutionDiviser();
		
		if(game.getLife() > 0){
		
			int playerSize = 96;
			if(xeon.getW() > xeon.getH()){
				playerSize = (int) (xeon.getH()/xeon.getResolutionDiviser()*0.20);
			}else{
				playerSize = (int) (xeon.getW()/xeon.getResolutionDiviser()*0.20);
			}
			
			int wl = (playerSize*2*game.getLife()/game.getDefaultLife());
			int ws = (playerSize*2*game.getSpecial()/100);
			
			if(game.getSpecial()>=100){
				g.drawImage(xeon.getAssetManager().getTexture("Missile_Logo_Loaded"), playerSize, gh-playerSize, playerSize, playerSize/2, null);
			}else{
				g.drawImage(xeon.getAssetManager().getTexture("Missile_Logo"), playerSize, gh-playerSize, playerSize, playerSize/2, null);
			}
			
			g.drawImage(xeon.getAssetManager().getTexture("Heart"), playerSize, gh-(playerSize/2), playerSize, playerSize/2, null);
			g.drawImage(xeon.getAssetManager().getTexture("Hitbox"), 2*playerSize, gh-(playerSize/2), wl, playerSize/2, null);
			g.drawImage(xeon.getAssetManager().getTexture("Special"), 2*playerSize, gh-playerSize, ws, playerSize/2, null);
			
			if(game.getLife()>game.getDefaultLife()/3*2){
				g.drawImage(xeon.getAssetManager().getTexture("Player"), 0, gh - playerSize, playerSize, playerSize, null);
			}else if(game.getLife()>game.getDefaultLife()/3 && game.getLife()<=game.getDefaultLife()/3*2){
				g.drawImage(xeon.getAssetManager().getTexture("PlayerHurt1"), 0, gh - playerSize, playerSize, playerSize, null);
			}else if(game.getLife() > 0){
				g.drawImage(xeon.getAssetManager().getTexture("PlayerHurt2"), 0, gh - playerSize, playerSize, playerSize, null);
			}
			
			
		}
		if(xeon.getWorldManager().getActiveWorld().getWorldName().equals("Main_Map")){
			g.setFont(xeon.getFontLoader().getFont("Venus", 20F));
			g.setColor(Color.white);
			String text = "  " + xeon.getLangManager().getLine("Score") + game.getScore();
			AffineTransform afftf = new AffineTransform();
			FontRenderContext frc =  new FontRenderContext(afftf, true, true);
			int txth = (int)(g.getFont().getStringBounds(text, frc).getHeight());
			g.drawString(text, 0, gh/10/2+txth/3);
		}
		
	}
	
}
