package Game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Random;

import Xeon.Xeon;

public class Starfield {

	private Game game;
	
	private Xeon xeon;
	
	private ArrayList<Star> stars = new ArrayList<Star>();
	
	private int offset = 0;
	private boolean isWidthLarger = false;
	
	public Starfield(Game game,Xeon xeon){
		
		this.game = game;
		this.xeon = xeon;
		
	}
	
	public void draw(Graphics2D g){
		
		for(int i = 0; i < stars.size(); i ++){
			
			g.setColor(stars.get(i).getColor());
			if(isWidthLarger){
				g.fillOval(stars.get(i).getX(), stars.get(i).getY() - offset, stars.get(i).getRadius(), stars.get(i).getRadius());
			}else{
				g.fillOval(stars.get(i).getX() - offset, stars.get(i).getY(), stars.get(i).getRadius(), stars.get(i).getRadius());
			}
			
		}
		
	}
	
	public void generateField(int quantity){
		
		int maxDim = 0;
		
		if(xeon.getW() > xeon.getH()){
			maxDim = xeon.getW()/xeon.getResolutionDiviser();
			isWidthLarger = true;
		}else{
			maxDim = xeon.getH()/xeon.getResolutionDiviser();
			isWidthLarger = false;
		}
		
		if(isWidthLarger){
			offset = (maxDim - (xeon.getH()/xeon.getResolutionDiviser()))/2;
		}else{
			offset = (maxDim - (xeon.getW()/xeon.getResolutionDiviser()))/2;
		}
		
		
		stars.clear();
		
		for(int i = 0; i < quantity; i ++){
			
			Random rand = new Random();
			int r = rand.nextInt(maxDim);
			Random rand1 = new Random();
			int r1 = rand1.nextInt(maxDim);
			Random rand2 = new Random();
			int r2 = rand2.nextInt(2)+1;
			
			float cr = (float) (rand.nextFloat() / 2f + 0.5);
			float cg = (float) (rand.nextFloat() / 2f + 0.5);
			float cb = (float) (rand.nextFloat() / 2f + 0.5);
			
			stars.add(new Star(r, r1, r2, new Color(cr,cg,cb)));
			
		}
		
	}
	
	
	
}
