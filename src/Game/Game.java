package Game;

import java.awt.Graphics2D;
import java.io.File;

import javax.swing.text.html.HTMLDocument.HTMLReader.SpecialAction;

import Assets.Music;
import Assets.SoundEffect;
import Core.Debugger;
import Entities.EntityFactory;
import Entity.Entity;
import Event.CollisionEvent;
import Event.CustomEvent;
import Event.EndOfMusicEvent;
import Event.EntityKillEvent;
import Event.RenderEvent;
import Event.TickEvent;
import Event.WorldEvent;
import Event.CollisionEvent.CollisionListener;
import Event.CustomEvent.CustomEventListener;
import Event.EndOfMusicEvent.EndOfMusicListener;
import Event.EntityKillEvent.KillListener;
import Event.RenderEvent.RenderListener;
import Event.TickEvent.TickListener;
import Event.WorldEvent.WorldEventListener;
import Xeon.Xeon;

public class Game {

public static Xeon xeon;
	
	private Level level;
	private Fx fx;
	private IA ia;
	private Collision collision;
	private Tick tick;
	private EntityFactory entityFactory;
	private Starfield starfield;
	private WaveManager waveManager;
	
	private float testColor = 0;
	
	private int loadingTitleCountDown = 3;
	
	private int life = 1500;
	private int defaultLife = 1500;
	private int score = 0;
	private int special = 100;
	
	public Game(){
		
		xeon = new Xeon("Defenders");
		
		level = new Level(this,xeon);
		
		fx = new Fx(xeon);
		
		ia = new IA(this, xeon);
		
		collision = new Collision(this, xeon);
		
		tick = new Tick(this, xeon);
		
		entityFactory = new EntityFactory(this, xeon);
		
		starfield = new Starfield(this, xeon);
		
		waveManager = new WaveManager(this, xeon);
		
		waveManager.loadWaves(new File("data/scripts/Waves.ww"));
		
		//Set all listeners
		
		setRender();
		
		setTick();
		
		setMisc();
		
		setWorld();
		
	}
	
	public void setRender(){
		
		xeon.getCentralEventManager().addRenderListener(new RenderListener() {
			
			@Override
			public void render(RenderEvent event) {

				if(xeon.getWorldManager().getActiveWorld().getWorldName().equals("Main_Map")){
					
					level.drawWorld(event.getGraphics());
					
				}
				
				
				
			}

			@Override
			public void renderOnTop(RenderEvent event) {
				
				level.drawGUI(event.getGraphics());

			}
		});
		
	}
	
	public void setTick(){
		
		xeon.getCentralEventManager().addTickListener(new TickListener() {
			
			@Override
			public void tick(TickEvent event) {

				tick.tick();
				
			}

			@Override
			public void slowTick(TickEvent event) {
				//System.out.println("tick");

				if(loadingTitleCountDown != 0){
					loadingTitleCountDown--;
					if(loadingTitleCountDown==0){
						xeon.getWorldManager().createChildWorld("Main_Menu");
					}
				}
				
				if(xeon.getWorldManager().getActiveWorld().getWorldName().equals("Main_Map")){
					//Check if there is no enemy left
					boolean hasEnemy = false;
					for(int i = 0; i < xeon.getEntityManager().getEntityList().size(); i++){
						Entity e = null;
						try{
							e = xeon.getEntityManager().getEntityList().get(i);
							if(e != null){
								if(e.getEntityType().equals("Enemy_Ship")){
									hasEnemy = true;
									break;
								}
							}
						}catch(Exception ex){
						}
					}
					if(!hasEnemy){
						waveManager.next();
					}
					addSpecial(1);
				}
				
			}
			
		});
		
	}
	
	public void setMisc(){
		
		xeon.getCentralEventManager().addCustomEventListener(new CustomEventListener() {
			
			@Override
			public void customEvent(CustomEvent event) {

				Debugger.log(event.getEventName());
				
			}
		});
		
		xeon.getCentralEventManager().addEndOfMusicEventListener(new EndOfMusicListener(){
			
			@Override
			public void endOfMusicReceived(EndOfMusicEvent event) {

				Debugger.log(event.getMusicAlias());
				
			}
			
		});
		
		xeon.getCentralEventManager().addCollisionEventListener(new CollisionListener() {
			
			@Override
			public void collide(CollisionEvent event) {
				
				//Debugger.log("Collision between " + event.getEntityA().getEntityType() + " and " + event.getEntityB().getEntityType());
				
				collision.collide(event);
				
			}
		});
		
		xeon.getCentralEventManager().addKillEventListener(new KillListener() {
			
			@Override
			public void killed(EntityKillEvent event) {
			
				//Debugger.log(event.getEntity().getEntityType() + " killed.");
				
			}
		});
		
	}
	
	public void setWorld(){
		
		xeon.getCentralEventManager().addWorldEventListener(new WorldEventListener() {
			
			@Override
			public void destroyed(WorldEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void created(WorldEvent event) {

				if(event.getWorld().getWorldName().equals("Main_Map")){
					life = defaultLife;
					score = 0;
					special = 100;
					xeon.getAssetManager().stopAllMusic();
					playMusicLoop("Airglow",false);
					waveManager.resetGame();
				}
				
				if(event.getWorld().getWorldName().equals("Main_Menu")){
					Debugger.log("MUSIC MUSIC MUSIC");
					playMusicLoop("Theme",false);
				}
				
			}
		});
		
	}
	
	public void playMusicLoop(String s,Boolean RestartIfPlaying){
		Music m = xeon.getAssetManager().getMusic(s);
		if(m != null){
			if(!RestartIfPlaying && m.getMusicPlayer().isPlaying()){
				return;
			}
			m.setDoesLoop(true);
			m.play();
		}
	}
	
	public void playSound(String s){
		SoundEffect e = xeon.getAssetManager().getEffect(s);
		if(e != null){
			e.play();
		}
	}
	
	public void addSpecial(int s){
		special = special + s;
		if(special>100){
			special = 100;
		}
	}

	public synchronized Level getLevel() {
		return level;
	}

	public synchronized void setLevel(Level level) {
		this.level = level;
	}

	public synchronized Fx getFx() {
		return fx;
	}

	public synchronized void setFx(Fx fx) {
		this.fx = fx;
	}

	public synchronized IA getIa() {
		return ia;
	}

	public synchronized void setIa(IA ia) {
		this.ia = ia;
	}

	public synchronized Collision getCollision() {
		return collision;
	}

	public synchronized void setCollision(Collision collision) {
		this.collision = collision;
	}

	public synchronized float getTestColor() {
		return testColor;
	}

	public synchronized void setTestColor(float testColor) {
		this.testColor = testColor;
	}

	public synchronized int getLoadingTitleCountDown() {
		return loadingTitleCountDown;
	}

	public synchronized void setLoadingTitleCountDown(int loadingTitleCountDown) {
		this.loadingTitleCountDown = loadingTitleCountDown;
	}

	public synchronized int getLife() {
		return life;
	}

	public synchronized void setLife(int life) {
		this.life = life;
	}

	public synchronized Tick getTick() {
		return tick;
	}

	public synchronized void setTick(Tick tick) {
		this.tick = tick;
	}

	public synchronized EntityFactory getEntityFactory() {
		return entityFactory;
	}

	public synchronized void setEntityFactory(EntityFactory entityFactory) {
		this.entityFactory = entityFactory;
	}

	public int getDefaultLife() {
		return defaultLife;
	}

	public void setDefaultLife(int defaultLife) {
		this.defaultLife = defaultLife;
	}

	public Starfield getStarfield() {
		return starfield;
	}

	public void setStarfield(Starfield starfield) {
		this.starfield = starfield;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public synchronized int getSpecial() {
		return special;
	}

	public synchronized void setSpecial(int special) {
		this.special = special;
	}
	
	
	
}
