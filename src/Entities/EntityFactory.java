package Entities;

import Entity.Entity;
import Entity.EntityParameterSet;
import Game.Game;
import Xeon.Xeon;

public class EntityFactory {

	private Game game;
	private Xeon xeon;
	
	public EntityFactory(Game game,Xeon xeon){
		
		this.game = game;
		this.xeon=xeon;
		
	}
	
	public Ship createSwift(int x, int y){
		
		EntityParameterSet eps = new EntityParameterSet();
		eps.setEntityName("Swift");
		eps.setEntityType("Enemy_Ship");
		eps.setX(x);
		eps.setY(y);
		eps.setW(60);
		eps.setH(36);
		eps.setRotation(0);
		eps.setTexture("Swift");
		eps.setXspeed(0);
		eps.setYspeed(0);
		eps.setFriction(0.05);
		eps.setDoclip(false);
		eps.setTimeToLive(0);
		eps.setEntityPhysic(Entity.PHYSIC_BOUNCY);
		
		return new Ship(xeon, eps, 1000);
		
	}
	
	public Ship createRaptor(int x, int y){
		
		EntityParameterSet eps = new EntityParameterSet();
		eps.setEntityName("Raptor");
		eps.setEntityType("Enemy_Ship");
		eps.setX(x);
		eps.setY(y);
		eps.setW(96);
		eps.setH(96);
		eps.setRotation(0);
		eps.setTexture("Raptor");
		eps.setXspeed(0);
		eps.setYspeed(0);
		eps.setFriction(0.05);
		eps.setDoclip(false);
		eps.setTimeToLive(0);
		eps.setEntityPhysic(Entity.PHYSIC_BOUNCY);
		
		return new Ship(xeon, eps, 4000);
		
	}
	
}
