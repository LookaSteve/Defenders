package Entities;

import Entity.Entity;
import Entity.EntityParameterSet;
import Xeon.Xeon;

public class Ship extends Entity{
	
	int life = 100;
	
	public Ship(Xeon xeon, EntityParameterSet eps, int life) {
		super(xeon, eps);
		this.life=life;
		
	}

	public synchronized int getLife() {
		return life;
	}

	public synchronized void setLife(int life) {
		this.life = life;
	}
	
}
