
//Assets loading script example
//To use with AssetLoader 1.0 or higher and Xéon 2.1.2 or higher

//Usage :
//			[<world alias>]
//			/TEXTURE [<alias>] <relative path>
//			/SOUND [<alias>] <relative path>
//			/MUSIC [<alias>] <relative path>

//Here the assets loaded by default such as buttons or menus...

[ALWAYS]
/MUSIC [Theme] data/assets/music/Defenders_Theme.ogg

[DEFAULT]

[Main_Menu]

//Here all the assets to load when you are in a specific world (in this case "Option Menu")
[Option_Menu]

[Demo_Menu]
/SOUND [Test_Sound] data/assets/sounds/testSound.wav

//Here all the assets to load when you are in a specific world (in this case "Main Map")
[Main_Map]
/TEXTURE [Condor] data/assets/textures/Condor.png
/TEXTURE [Earth] data/assets/textures/Earth.png
/TEXTURE [Moon] data/assets/textures/Moon_Shaded.png
/TEXTURE [Hitbox] data/assets/textures/Hitbox.png
/TEXTURE [Player] data/assets/textures/Player.png
/TEXTURE [PlayerHurt1] data/assets/textures/Player_Hurt_1.png
/TEXTURE [PlayerHurt2] data/assets/textures/Player_Hurt_2.png
/TEXTURE [Shot] data/assets/textures/Shot.png
/TEXTURE [Power_Shot] data/assets/textures/Power_Shot.png
/TEXTURE [Swift] data/assets/textures/Swift.png
/TEXTURE [Missile_Logo_Loaded] data/assets/textures/Missile_Logo_Loaded.png
/TEXTURE [Missile_Logo] data/assets/textures/Missile_Logo.png
/TEXTURE [Heart] data/assets/textures/Heart.png
/TEXTURE [Special] data/assets/textures/Special.png
/TEXTURE [Raptor] data/assets/textures/Raptor.png

/ANIMATION [Explosion] data/assets/textures/explosion/ 10
/ANIMATION [Small_Explosion] data/assets/textures/small_explosion/ 10
/ANIMATION [Misile] data/assets/textures/misile/ 10
/ANIMATION [Ion_Blast] data/assets/textures/ion_blast/ 5
/ANIMATION [Health_Bonus] data/assets/textures/health_bonus/ 10

/TEXTURE [Debris_1] data/assets/textures/debris/debris_1.png
/TEXTURE [Debris_2] data/assets/textures/debris/debris_2.png
/TEXTURE [Debris_3] data/assets/textures/debris/debris_3.png
/TEXTURE [Debris_4] data/assets/textures/debris/debris_4.png
/TEXTURE [Debris_5] data/assets/textures/debris/debris_5.png
/TEXTURE [Debris_6] data/assets/textures/debris/debris_6.png

/MUSIC [Airglow] data/assets/music/Stellardrone_Airglow.ogg
/MUSIC [Game_Over] data/assets/music/Game_Over.ogg

/EFFECT [Laser] data/assets/sounds/Laser.ogg
/EFFECT [Boom] data/assets/sounds/Boom.ogg
/EFFECT [Missile] data/assets/sounds/Missile.ogg
/EFFECT [Ion] data/assets/sounds/Ion.ogg

//Assets you can load at any time and for any reason
[TOOLS]
