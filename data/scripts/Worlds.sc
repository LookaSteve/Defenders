
//World loading script example
//To use with WorldManager 1.0 or higher and Xéon 2.1.2 or higher

//Usage :
//			[<world alias>]
//			/TYPE <type> 					GUI (menu) or GAMEMAP (a screen with the game displayed)
//			/SETUP <relative path>			Path to the GUI configuration script
//			/ONLOAD <relative path>			Path to an Actionscript that will be executed when the world loads
//			/LIGHTENGINE <boolean>			Use the light engine
//			/CONTROLLER <boolean>			Use the controller (keyboard or gamepad)
//			/BACKGROUNDCOLOR <#color>		Set background color
//			/CONTROLLER_ROTATION <boolean>	Player control over the rotation
//			/ROTATION <double>				Default rotation
//			/ROTATION_SPEED <double>		Default rotation speed
//			/XPOSITION <double>				Default x position
//			/YPOSITION <double>				Default y position
//			/CAMERA_SPEED <double>			Default speed of the camera
//			/UNLOAD_ONCHILD <boolean>		Has to unload the assets of this world when a child is created
//			/NOCLIP <boolean>				Can the camre go trough collision blocs ?
//			/CLIP_WIDTH <int>				Width of the camera's hitbox
//			/CLIP_HEIGHT <int>				Height of the camera's hitbox
	
//Here all the worlds are defined

[DEFAULT]
/TYPE GUI
/BACKGROUNDCOLOR #000000
/SETUP data/scripts/GUI/Splash.gui
/UNLOAD_ONCHILD true

[Main_Menu]
/TYPE GUI
/BACKGROUNDCOLOR #000000
/SETUP data/scripts/GUI/Main_Menu.gui
/UNLOAD_ONCHILD true

[Option_Menu]
/TYPE GUI
/SETUP data/scripts/GUI/Option_Menu.gui

[Keybind_Menu]
/TYPE GUI
/SETUP data/scripts/GUI/Keybind_Menu.gui

[Langchooser_Menu]
/TYPE GUI
/SETUP data/scripts/GUI/Langchooser_Menu.gui

[Credits]
/TYPE GUI
/SETUP data/scripts/GUI/Credits.gui

[Main_Map]
/TYPE GAMEMAP
/ONLOAD data/scripts/ActionScripts/Demo_Lights.sc
/SETUP data/scripts/GUI/Main_Map.gui
/CONTROLLER true
/LIGHTENGINE false
/BACKGROUNDCOLOR #000000
/CONTROLLER_ROTATION true
/ROTATION 0
/ROTATION_SPEED 0.1
/XPOSITION 0
/YPOSITION 0
/CAMERA_SPEED 5
/UNLOAD_ONCHILD true
/CLIP true
/CLIP_WIDTH 51
/CLIP_HEIGHT 66
/VIEWPORT_SIZE 500
/CLIP_TEXTURE Condor
/CLIP_ROTATION 0
/ROTATION_TYPE FOCUSED_ROTATION
/CLIP_FOLLOWMOUSE true
/CLIP_FOLLOWMOUSE_OFFSET 3.141592653589793238