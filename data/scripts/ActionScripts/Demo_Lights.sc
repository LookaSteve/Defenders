
//Set ambiant light
/SETAMBIANTLIGHT #505050

//LIGHTSOURCE COMMAND : /LIGHTSOURCE <alias> <x> <y> <radius> <intensity%> <color hex-code> <world_alias> <is active>
/LIGHTSOURCE id=Demo_Light x=0 y=0 radius=300 intensity=100 color=#ff0000 worldname=Main_Map active=true
/LIGHTSOURCE id=Demo_Light2 x=-150 y=-150 radius=300 intensity=50 color=#00ff00 worldname=Main_Map active=true
/LIGHTSOURCE id=Demo_Light3 x=150 y=-150 radius=300 intensity=100 color=#0000ff worldname=Main_Map active=true
/LIGHTSOURCE id=Demo_Light4 x=500 y=500 radius=300 intensity=100 color=#9A32CD worldname=Main_Map active=true

//Refreshes the light list (necessary to have a good light display)
/REFRESHWORLD

//Demo blink

/WAIT 500

/TOGGLELIGHT Demo_Light

/WAIT 500

/TOGGLELIGHT Demo_Light

/WAIT 500

/TOGGLELIGHT Demo_Light

/WAIT 500

/TOGGLELIGHT Demo_Light

/WAIT 500

/SENDCUSTOMEVENT TEST_EVENT

///////////////////////TEST NEW COMMANDS
//SETCAMERAX 500
//SETCAMERAY 500
//SETROTATION 5
//SETROTATIONSPEED 0.1
//SETCAMERASPEED 40
//SETCONTROLLER disabled
//SETCONTROLLERROT disabled
